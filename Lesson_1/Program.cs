﻿using System;

namespace Lesson_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 72;
            int b = a - 22;
            int c = a + b;
            Console.WriteLine("1.Сумма двух переменных равна = " + c + "\n");

            float weight = 10.2f;
            float height = weight / 2;
            float Sp = (weight + height) * 2;
            Console.WriteLine("2.Площадь прямоугольника по длине и ширине равна = " + Sp + "\n");

            double PI = 3.14f;
            Console.WriteLine("3.Введите радиус:");
            double radius = Convert.ToDouble(Console.ReadLine());
            double Srk = PI * (radius * radius);
            Console.WriteLine("3.Площадь круга = " + Srk + "\n");

            Console.WriteLine("4.Введите целое число:");
            double chislo = Convert.ToDouble(Console.ReadLine());
            if(chislo % 2 == 0)
            {
                Console.WriteLine("4.Данное число чётное.\n");
            }
            else
            {
                Console.WriteLine("4.Данное число нечётное.\n");
            }

            float[] numbers = { 3.5f, 5, 7.1f, 9.7f, 1 };
            float summ = numbers[1] + numbers[2] + numbers[3] + numbers[4] + numbers[0];
            Console.WriteLine("5.Сумма элементов массива равна = " + summ);

         
        }
    }
}
